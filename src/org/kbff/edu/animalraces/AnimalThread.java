package org.kbff.edu.animalraces;

public class AnimalThread extends Thread {
	private int meters;
	
	public AnimalThread(String name, int priority) {
		setName(name);
		setPriority(priority);
		meters = 0;
	}
	
	public void run() {
		while(meters < 300_000) {
			if(meters % 10_000 == 0) {
				System.out.printf("%s: %d\n", getName(), meters);
				try {
					sleep(550);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			
			meters++;
			
			if(meters % 151_000 == 0) {
				if(getPriority() < 5) {
					System.out.printf("У %s открывается второе дыхание!\n", getName());
					setPriority(Thread.MAX_PRIORITY);
				} else {
					System.out.printf("%s сдулся и падает в травку без сил!\n", getName());
					setPriority(Thread.MIN_PRIORITY);
				}
			}
		}
		System.out.printf("%s финишировал!\n", getName());
	}
	
	public int getMeters() {
		return meters;
	}
}
