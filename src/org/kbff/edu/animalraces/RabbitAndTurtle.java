package org.kbff.edu.animalraces;


public class RabbitAndTurtle {

	public static void main(String[] args) throws InterruptedException {
		AnimalThread rabbit = new AnimalThread("Rabbit", 10);
		AnimalThread turtle = new AnimalThread("Turtle", 1);
		
		rabbit.start();
		turtle.start();
	}

}
